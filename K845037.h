#ifndef K845037_h
#define K845037_h

class  K845037 {

public:

 K845037 (int analogChanel, int digitalChn_1, int digitalCnn_2,int digitalChn_3,int digitalChn_4,int digitalChn_5);

void lettura_zero();
void pulsante_Sw1();
void pulsante_Sw2();
void pulsante_Sw3();
void pulsante_Sw4();
void pulsante_Sw5();

int analog;
int digital_1; 
int digital_2; 
int digital_3; 
int digital_4; 
int digital_5; 
int _soglia;
int digitalPinState_1;
int digitalPinState_2;
int digitalPinState_3;
int digitalPinState_4;
int digitalPinState_5;

long lastDebounceTime = 0; // l'ultima volta che il pin di uscita è stato attivato
long debounceDelay = 500;    // il tempo di debounce; aumenta se c'è sempre sfarfalio

private:

};

#endif
