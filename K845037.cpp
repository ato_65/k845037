#include "K845037.h"
#include "Arduino.h"

#define DEBUG




K845037::K845037(int analogChanel, int digitalChn_1, int digitalChn_2, int digitalChn_3, int digitalChn_4, int digitalChn_5 ):
  analog(analogChanel), digital_1(digitalChn_1), digital_2(digitalChn_2), digital_3(digitalChn_3), digital_4(digitalChn_4), digital_5(digitalChn_5)
{
  pinMode (analogChanel, INPUT);
  pinMode (digitalChn_1, OUTPUT);
  pinMode (digitalChn_2, OUTPUT);
  pinMode (digitalChn_3, OUTPUT);
  pinMode (digitalChn_4, OUTPUT);
  pinMode (digitalChn_5, OUTPUT);
  // inizializzo tutte le uscite HIGH (spente)
  digitalWrite(digital_1, HIGH);
  digitalWrite(digital_2, HIGH);
  digitalWrite(digital_3, HIGH);
  digitalWrite(digital_4, HIGH);
  digitalWrite(digital_5, HIGH);
  // inizializzo tutti gli stati dei pulsanti HIGH
  digitalPinState_1 = HIGH;
  digitalPinState_2 = HIGH;
  digitalPinState_3 = HIGH;
  digitalPinState_4 = HIGH;
  digitalPinState_5 = HIGH;


}



void K845037::lettura_zero()

{

  _soglia = analogRead (analog);
  //Serial.println (_soglia);// per debug

  // analog condizioni normali il nessun pulsante premuto il livello OUT è alto quindi leggo 1023
  if (_soglia >= 1000 && _soglia <= 1023) {
    // non fa niente
  }
}


void  K845037::pulsante_Sw1()


{
    _soglia = analogRead (analog);
#ifdef DEBUG
  Serial.println (_soglia);
  Serial.println (digitalPinState_1);
#endif
  //se il valore analog è compreso fra 0 e 90 ho premuto lo SW1
  if (_soglia != 0 && _soglia <= 90) { //se il valore analog è compreso fra 0 e 50 ho premuto lo SW1
    if (digitalPinState_1 == HIGH) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {

        Serial.println("SW1");
        digitalWrite(digital_1, LOW);
        digitalPinState_1 = LOW;
        lastDebounceTime = millis();
       
      }
    }

    else if (digitalPinState_1 == LOW) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        digitalWrite(digital_1, HIGH);
        digitalPinState_1 = HIGH;
        lastDebounceTime = millis();
       
      }
    }
  }
}

void  K845037::pulsante_Sw2()
{
  _soglia = analogRead (analog);
  //Serial.println (_soglia);// per debug

  //se il valore analog è compreso fra 100 e 220 ho premuto lo SW2
  if (_soglia >= 100 && _soglia <= 220) {
    if (digitalPinState_2 == HIGH) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        //Serial.println("SW2");
        digitalWrite(digital_2, LOW);
        digitalPinState_2 = LOW;
        lastDebounceTime = millis();
      }
    }

    else if (digitalPinState_2 == LOW) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        digitalWrite(digital_2, HIGH);
        digitalPinState_2 = HIGH;
        lastDebounceTime = millis();
      }
    }
  }
}

void  K845037::pulsante_Sw3()
{
  _soglia = analogRead (analog);
  //Serial.println (_soglia);// per debug

  //se il valore analog è compreso fra 300 e 430 ho premuto lo SW3
  if (_soglia >= 300 && _soglia <= 430) {
    if ((millis() - lastDebounceTime ) > debounceDelay) {
      if (digitalPinState_3 == HIGH) {
        //Serial.println("SW3");
        digitalWrite(digital_3, LOW);
        digitalPinState_3 = LOW;
        lastDebounceTime = millis();
      }
    }

    else if (digitalPinState_3 == LOW) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        digitalWrite(digital_3, HIGH);
        digitalPinState_3 = HIGH;
        lastDebounceTime = millis();
      }
    }
  }
}

void  K845037::pulsante_Sw4()
{
  _soglia = analogRead (analog);
  //Serial.println (_soglia);// per debug

  //se il valore analog è compreso fra 450 e 599 ho premuto lo SW4
  if (_soglia >= 450 && _soglia <= 599) {
    if ((millis() - lastDebounceTime ) > debounceDelay) {
      if (digitalPinState_4 == HIGH) {
        //Serial.println("SW4");
        digitalWrite(digital_4, LOW);
        digitalPinState_4 = LOW;
        lastDebounceTime = millis();
      }
    }

    else if (digitalPinState_4 == LOW) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        digitalWrite(digital_4, HIGH);
        digitalPinState_4 = HIGH;
        lastDebounceTime = millis();
      }
    }
  }
}

void  K845037::pulsante_Sw5()
{
  _soglia = analogRead (analog);
  //Serial.println (_soglia);// per debug

  //se il valore analog è compreso fra 650 e 750 ho premuto lo SW5
  if (_soglia >= 650 && _soglia <= 750) {
    if (digitalPinState_5 == HIGH) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        //Serial.println("SW5");
        digitalWrite(digital_5, LOW);
        digitalPinState_5 = LOW;
        lastDebounceTime = millis();
      }
    }
    else if (digitalPinState_5 == LOW) {
      if ((millis() - lastDebounceTime ) > debounceDelay) {
        digitalWrite(digital_5, HIGH);
        digitalPinState_5 = HIGH;
        lastDebounceTime = millis();

      }
    }
  }
}
