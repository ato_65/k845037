

// questa funzione serve a Blynk per aggiornare lo stato dei pulsanti sul terminale android
void inviaMisura() {


  Blynk.run(); // esegue Blynk

  int digitalPinState_1 = digitalRead (D1);
  int digitalPinState_2 = digitalRead (D2);
  int digitalPinState_3 = digitalRead (D3);
  int digitalPinState_4 = digitalRead (D4);
  //int digitalPinState_5 = digitalRead (D5);


  Blynk.virtualWrite(V1, digitalPinState_1);
  Blynk.virtualWrite(V2, digitalPinState_2);
  Blynk.virtualWrite(V3, digitalPinState_3);
  Blynk.virtualWrite(V4, digitalPinState_4);
  //Blynk.virtualWrite(V5, digitalPinState_5);



}
