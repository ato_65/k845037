**# k845037**

**Pulsantiera analogica 5 pulsanti**

Questo progetto è puramente un esercizio teorico per lo studio di una 
programmazione a oggetti.

Fa parte di un progetto di studio del Gruppo Utenti Linux Livorno (Gulli).

Breve descrizione del progetto:
si utilizza una pulsantiera di 5 pulsanti ([k845037](https://www.google.com/url?sa=i&url=https%3A%2F%2Feasyeda.com%2Fmodules%2F5-Keyes-AD-Key-K845037_679f3b0ef1ad4c36b8c8c03d79af195a&psig=AOvVaw2FTKrJvKSh6yANMOkDVrtZ&ust=1591765468914000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKjijNH68-kCFQAAAAAdAAAAABAD)), che tramite un partitore resistivo
è connessa all'Arduino [(Wemos D1 mini](https://www.homotix.it/articoli/introduzione-alla-wemos-mini)),
utilizzando il solo ingresso analogico
riusciremo a pilotare diverse uscite digitali (connessi a dei rele).


L'idea per il progetto definitivo sarebbe quello di realizzare una pulsantiera
per gestire dei rele da locale (pulsante accendi/spegni utenza), trasmettendo
lo stato delle uscite digitali verso il server Blynk in modo da gestire il 
tutto anche da remoto.

Questo progetto è un draft in corso di implementazione
24-Giugno-2020 Implementato Debounce ai 5 tasti
il progetto è funzionante ed integrato con Arduino.