/*
   --------------------------------------------------------------------------
   5_pulsanti_24-06-2020 1.1
   Autore Mauro Ricciardi
   data 24-Giugno-2020
   contatti: mauro.ricciardi1965@gmail.com
   ultima modifica:inserimento debounce sui pulsanti
   wemos D1 mini

   --------------------------------------------------------------------------
*/
#define DEBUG

#include "K845037.h"
#define BLYNK_PRINT Serial


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "XXXXXXXXXXXXXXXXXXXXXXX"; // inserisci il codice blynk associato al device


// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "AAAAAAAAAAA"; // inserisci la rete a cui connettere WIFI il device
char pass[] = "YYYYYYY"; // inserisci la tua password della rete WIFI

K845037 sw(0, D1, D2, D3, D4, D5); //input A0 e output D1 to D5


void setup ()
{
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("DEBUG is active");
  delay(500);
#endif

  Blynk.begin(auth, ssid, pass, IPAddress(192, 168, 1, 31), 8080);

}

void loop()
{
  Blynk.run();
  sw.pulsante_Sw1();
  sw.pulsante_Sw2();
  sw.pulsante_Sw3();
  sw.pulsante_Sw4();
  //sw.pulsante_Sw5(); // non utilizzato per il momento
  inviaMisura();
  
}
